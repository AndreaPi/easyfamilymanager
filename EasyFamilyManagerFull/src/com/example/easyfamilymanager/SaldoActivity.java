package com.example.easyfamilymanager;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SaldoActivity extends Activity {
	ArrayList<String> idVoci = new ArrayList<String>();
	List<Padre> listaPadriFull;
	List<Figlio> listaFigliFull;
	Map<Padre, List<Figlio>> mappaFull;
	ExpandableListView expListView;
	Cursor voce;
	ExpandableListAdapter expListAdapter;
	Activity context;

	@SuppressLint("ResourceAsColor")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pagina_saldo);

		// scrivo il nome utente
		VariabiliGlobali globali = (VariabiliGlobali) this.getApplication();
		TextView campoUtente = (TextView) findViewById(R.id.editUtente);
		campoUtente.setText(globali.getUtente());

		// bottone pagina home
		ImageButton bottoneHome = (ImageButton) findViewById(R.id.bottoneHome);
		bottoneHome.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaHome = new Intent(SaldoActivity.this, HomeActivity.class);
				startActivity(openPaginaHome);
				finish();
			}
		});

		// bottone pagina indietro
		ImageButton bottoneIndietro = (ImageButton) findViewById(R.id.bottoneIndietro);
		bottoneIndietro.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaHome = new Intent(SaldoActivity.this, HomeActivity.class);
				startActivity(openPaginaHome);
				finish();
			}
		});

		// gestisco scomparsa menu filtri
		final RelativeLayout filtri = (RelativeLayout) findViewById(R.id.filtri);
		filtri.setVisibility(View.GONE);
		final ImageButton bottoneMenu = (ImageButton) findViewById(R.id.bottoneMenu);
		bottoneMenu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (filtri.getVisibility() == View.GONE) {
					bottoneMenu.setImageResource(R.drawable.up);
					filtri.setVisibility(View.VISIBLE);
				} else {
					bottoneMenu.setImageResource(R.drawable.down);
					filtri.setVisibility(View.GONE);
				}
			}
		});

		// apro e leggo DB
		TextView campoSaldo = (TextView) findViewById(R.id.totale);
		CustomDbHelper databaseHelper = new CustomDbHelper(getApplicationContext());
		SQLiteDatabase db = databaseHelper.getReadableDatabase();
		String sql = "SELECT rate.idmovimento, nomerata, substr(datarata,1,4) anno, substr(datarata,5,2) mese, substr(datarata,7,2) giorno, importo, rischio, descrizione, idcategoria "
				+ "FROM rate INNER JOIN movimenti ON rate.idmovimento = movimenti.idmovimento "
				+ "WHERE username = '" + globali.getUtente() + "' ORDER BY datarata ASC";
		voce = db.rawQuery(sql, null);
		sql = "SELECT idcategoria, nomecategoria FROM categorie WHERE username = '" + globali.getUtente() + "' ORDER BY nomecategoria ASC";
		Cursor categoria = db.rawQuery(sql, null);

		// inserisco nelle strutture dati
		mappaFull = new LinkedHashMap<Padre, List<Figlio>>();
		listaPadriFull = new ArrayList<Padre>();
		float totale = 0;
		while (categoria.moveToNext()) {
			float parziale = 0;
			listaFigliFull = new ArrayList<Figlio>();
			if (voce.moveToFirst()) {
				do {
					if (voce.getString(voce.getColumnIndex("idcategoria")) != null)
						if (voce.getString(voce.getColumnIndex("idcategoria")).equals(categoria.getString(categoria.getColumnIndex("idcategoria")))) {
							float importoReale = Float.parseFloat(voce.getString(voce.getColumnIndex("importo"))) * Float.parseFloat(voce.getString(voce.getColumnIndex("rischio"))) / 100;
							Figlio figlio = new Figlio(
									voce.getString(voce.getColumnIndex("idmovimento")),
									voce.getString(voce.getColumnIndex("nomerata")),
									voce.getString(voce.getColumnIndex("giorno")) + "/" + voce.getString(voce.getColumnIndex("mese")) + "/" + voce.getString(voce.getColumnIndex("anno")),
									Float.toString(importoReale),
									voce.getString(voce.getColumnIndex("rischio")),
									voce.getString(voce.getColumnIndex("descrizione")));
							listaFigliFull.add(figlio);
							parziale += importoReale;
						}
				} while (voce.moveToNext());
			}
			Padre padre = new Padre(categoria.getString(categoria.getColumnIndex("nomecategoria")), Float.toString(parziale));
			listaPadriFull.add(padre);
			mappaFull.put(padre, listaFigliFull);
			totale += parziale;
		}

		// gestisco il caso in cui la categoria selezionata � quella di default
		listaFigliFull = new ArrayList<Figlio>();
		float parziale = 0;
		if (voce.moveToFirst()) {
			do {
				if (voce.getString(voce.getColumnIndex("idcategoria")) == null) {
					float importoReale = Float.parseFloat(voce.getString(voce.getColumnIndex("importo"))) * Float.parseFloat(voce.getString(voce.getColumnIndex("rischio"))) / 100;
					Figlio figlio = new Figlio(
							voce.getString(voce.getColumnIndex("idmovimento")),
							voce.getString(voce.getColumnIndex("nomerata")),
							voce.getString(voce.getColumnIndex("giorno")) + "/" + voce.getString(voce.getColumnIndex("mese")) + "/" + voce.getString(voce.getColumnIndex("anno")),
							Float.toString(importoReale),
							voce.getString(voce.getColumnIndex("rischio")), 
					voce.getString(voce.getColumnIndex("descrizione")));
					listaFigliFull.add(figlio);
					parziale += importoReale;
				}
			} while (voce.moveToNext());
		}
		Padre padre = new Padre(globali.getCategoriaRoot(), Float.toString(parziale));
		listaPadriFull.add(padre);
		mappaFull.put(padre, listaFigliFull);
		totale += parziale;

		// visualizzo il saldo con il colore adeguato
		if (totale > 0)
			campoSaldo.setTextColor(getResources().getColor(R.color.positivo));
		if (totale < 0)
			campoSaldo.setTextColor(getResources().getColor(R.color.negativo));
		campoSaldo.setText(Float.toString(totale));

		// creo la lista
		expListView = (ExpandableListView) findViewById(R.id.lista);
		expListAdapter = new ExpandableListAdapter(this, listaPadriFull, mappaFull);
		expListView.setAdapter(expListAdapter);

		// prendo i dati per costruire i filtri
		final DatePicker minData = (DatePicker) this.findViewById(R.id.editDal);
		final DatePicker maxData = (DatePicker) this.findViewById(R.id.editAl);
		final TextView labelMinData = (TextView) this.findViewById(R.id.labelDal);
		final TextView labelMaxData = (TextView) this.findViewById(R.id.labelAl);
		final CheckBox minDataOn = (CheckBox) this.findViewById(R.id.disabilitaDal);
		final CheckBox maxDataOn = (CheckBox) this.findViewById(R.id.disabilitaAl);
		final EditText minImporto = (EditText) this.findViewById(R.id.minoreImporto);
		final EditText maxImporto = (EditText) this.findViewById(R.id.maggioreImporto);
		final EditText parolaChiave = (EditText) this.findViewById(R.id.editChiave);
		final CheckBox inNome = (CheckBox) this.findViewById(R.id.inNome);
		final CheckBox inDescrizione = (CheckBox) this.findViewById(R.id.inDescrizione);
		minData.setEnabled(false);
		labelMinData.setEnabled(false);
		maxData.setEnabled(false);
		labelMaxData.setEnabled(false);
		
		// checkbox data Dal
		minDataOn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(minDataOn.isChecked()){
					minData.setEnabled(true);
					labelMinData.setEnabled(true);
				}else{
					minData.setEnabled(false);
					labelMinData.setEnabled(false);
				}
			}
		});
		
		// checkbox data Al
		maxDataOn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(maxDataOn.isChecked()){
					maxData.setEnabled(true);
					labelMaxData.setEnabled(true);
				}else{
					maxData.setEnabled(false);
					labelMaxData.setEnabled(false);
				}
			}
		});
		
		// pulsante filtri
		context=this;
		Button bottoneApplicaFiltri = (Button) findViewById(R.id.applicaFiltri);
		bottoneApplicaFiltri.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				
				// creo il filtro prendendo i dati dall'interfaccia
				String minDataAttiva;
				if(minData.isEnabled())
					minDataAttiva = String.format("%04d", minData.getYear()) + String.format("%02d", 1 + minData.getMonth()) + String.format("%02d", minData.getDayOfMonth());
				else
					minDataAttiva = null;
				String maxDataAttiva;
				if(maxData.isEnabled())
					maxDataAttiva = String.format("%04d", maxData.getYear()) + String.format("%02d", 1 + maxData.getMonth()) + String.format("%02d", maxData.getDayOfMonth());
				else
					maxDataAttiva = null;
				Filtro filtro = new Filtro(
						minDataAttiva, maxDataAttiva,
						minImporto.getText().toString(), maxImporto.getText().toString(),
						parolaChiave.getText().toString(), inNome.isChecked(), inDescrizione.isChecked());
				
				// logica per la scansione
				Map<Padre, List<Figlio>> mappaFiltrata = new LinkedHashMap<Padre, List<Figlio>>();
				List<Padre> listaPadriFiltrata = new ArrayList<Padre>();
				Figlio figlio;
				for (Entry<Padre, List<Figlio>> elementoCorrente : mappaFull.entrySet()) {
					Padre padre = elementoCorrente.getKey();
					List<Figlio> figli = elementoCorrente.getValue();
					ArrayList<Figlio> listaFigliFiltrata = new ArrayList<Figlio>();
					for (int i = 0; i < figli.size(); i++) {
						figlio = figli.get(i);
						if (figlio.getFiltrato(filtro)) {
							listaFigliFiltrata.add(figlio);
						}
					}
					if(listaFigliFiltrata.size() > 0)
					{
						mappaFiltrata.put(padre, figli);
						listaPadriFiltrata.add(padre);
					}
				}
				expListAdapter = new ExpandableListAdapter(context, listaPadriFiltrata, mappaFiltrata);
				expListView.setAdapter(expListAdapter);
				bottoneMenu.setImageResource(R.drawable.down);
				filtri.setVisibility(View.GONE);
				Toast.makeText(getApplicationContext(), R.string.conferma_filtri, Toast.LENGTH_SHORT).show();
			}
		});

		// click sulle voci
		expListView.setOnChildClickListener(new OnChildClickListener() {
			public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
				final Long idSelezionato = expListAdapter.getChildId(groupPosition, childPosition);
				final Float rischio = expListAdapter.getRischio(groupPosition, childPosition);
				final String nome = expListAdapter.getNome(groupPosition, childPosition);

				// chiamo la pagina modifica
				voce.moveToFirst();
				if (rischio == 100) {
					if (nome.contains("[")) {
						Intent openPaginaModificaMovimento = new Intent(getApplicationContext(), ModificaMovimentoActivity.class);
						openPaginaModificaMovimento.putExtra(getPackageName() + ".parametroId", idSelezionato.toString());
						startActivity(openPaginaModificaMovimento);
						finish();
					} else {
						Intent openPaginaModificaSemplice = new Intent(getApplicationContext(), ModificaSempliceActivity.class);
						openPaginaModificaSemplice.putExtra(getPackageName() + ".parametroId", idSelezionato.toString());
						startActivity(openPaginaModificaSemplice);
						finish();
					}
				} else {
					Intent openPaginaModificaRischio = new Intent(getApplicationContext(), ModificaRischioActivity.class);
					openPaginaModificaRischio.putExtra(getPackageName() + ".parametroId", idSelezionato.toString());
					startActivity(openPaginaModificaRischio);
					finish();
				}
				return true;
			}
		});
		
		// coloro il testo dell'importo minore
		minImporto.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try{
				if (Float.parseFloat(minImporto.getText().toString()) > 0)
					minImporto.setTextColor(R.color.positivo);
				if (Float.parseFloat(minImporto.getText().toString()) < 0)
					minImporto.setTextColor(R.color.negativo);
				if (Float.parseFloat(minImporto.getText().toString()) == 0)
					minImporto.setTextColor(R.color.abc_search_url_text_pressed);
				} catch (NumberFormatException e) {
					minImporto.setTextColor(context.getResources().getColor(R.color.abc_search_url_text_pressed));
				}
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
			}
		});
		
		// coloro il testo dell'importo maggiore
		maxImporto.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try{
				if (Float.parseFloat(maxImporto.getText().toString()) > 0)
					maxImporto.setTextColor(R.color.positivo);
				if (Float.parseFloat(maxImporto.getText().toString()) < 0)
					maxImporto.setTextColor(R.color.negativo);
				if (Float.parseFloat(maxImporto.getText().toString()) == 0)
					maxImporto.setTextColor(R.color.abc_search_url_text_pressed);
				} catch (NumberFormatException e) {
					maxImporto.setTextColor(context.getResources().getColor(R.color.abc_search_url_text_pressed));
				}
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
			}
		});
	}
}