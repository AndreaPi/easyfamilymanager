package com.example.easyfamilymanager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CustomDbHelper extends SQLiteOpenHelper {

	public CustomDbHelper(Context context) {
		super(context, "archivio", null, 1);
	}

	// creazione del DB
	@Override
	public void onCreate(SQLiteDatabase db) {

		String sql = "CREATE TABLE movimenti ("
				+ "idmovimento INTEGER PRIMARY KEY, "
				+ "nomemovimento STRING NOT NULL, "
				+ "datamovimento STRING NOT NULL, "
				+ "descrizione STRING, "
				+ "idcategoria INTEGER, "
				+ "ricorrenza STRING, "
				+ "rischio REAL NOT NULL, "
				+ "username STRING NOT NULL, "
				+ "FOREIGN KEY(idcategoria) REFERENCES categorie(idcategoria), "
				+ "FOREIGN KEY(username) REFERENCES utenti(username)"
				+ ");";
		db.execSQL(sql);

		sql = "CREATE TABLE rate ("
				+ "idrata INTEGER PRIMARY KEY, "
				+ "nomerata STRING NOT NULL, "
				+ "datarata STRING NOT NULL, "
				+ "importo REAL NOT NULL, "
				+ "idmovimento INTEGER,"
				+ "idallarme INTEGER,"
				+ "FOREIGN KEY(idmovimento) REFERENCES movimenti(idmovimento)"
				+ ");";
		db.execSQL(sql);

		sql = "CREATE TABLE utenti ("
				+ "username STRING NOT NULL, "
				+ "password STRING NOT NULL, "
				+ "autologin INTEGER, "
				+ "oraallarme STRING "
				+ ");";
		db.execSQL(sql);

		sql = "CREATE TABLE categorie ("
				+ "idcategoria INTEGER PRIMARY KEY, "
				+ "nomecategoria STRING NOT NULL, "
				+ "username STRING "
				+ ");";
		db.execSQL(sql);
		for (int i = 0; i < VariabiliGlobali.precostruite.length; i++) {
			sql = "INSERT INTO categorie (nomecategoria, username) VALUES ('" + VariabiliGlobali.precostruite[i] + "', '" + VariabiliGlobali.UTENTE_DEFAULT + "');";
			db.execSQL(sql);
		}

		sql = "CREATE TABLE allarmi ("
				+ "idallarme INTEGER PRIMARY KEY"
				+ ");";
		db.execSQL(sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub

	}
}
