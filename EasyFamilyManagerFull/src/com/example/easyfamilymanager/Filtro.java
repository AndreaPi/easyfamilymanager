package com.example.easyfamilymanager;

public class Filtro {
	private String mindata; // formato YYYYMMDD
	private String maxdata;
	private String minimporto;
	private String maximporto;
	private String parolachiave;
	private boolean inNome;
	private boolean inDescrizione;

	public Filtro(String dataMin, String dataMax, String importoMin, String importoMax, String keyword, Boolean nome, Boolean descrizione) {
		this.mindata = dataMin;
		this.maxdata = dataMax;
		this.minimporto = importoMin;
		this.maximporto = importoMax;
		this.parolachiave = keyword;
		this.inNome = nome;
		this.inDescrizione = descrizione;
	}

	public boolean getFiltrato(Figlio figlio) {
		if (minDataValida(figlio.getData(), this.mindata) && maxDataValida(figlio.getData(), this.maxdata) &&
				minImportoValido(figlio.getImporto(), this.minimporto) && maxImportoValido(figlio.getImporto(), this.maximporto)
				&& inNomeValido(figlio.getNome(), this.parolachiave, this.inNome) && inDescrizioneValido(figlio.getDescrizione(), this.parolachiave, this.inDescrizione))
			return true;
		else
			return false;
	}

	private boolean minDataValida(String data, String minData) { // formato DD/MM/YYYY
		String dataCorretta = data.substring(6, 10) + data.substring(3, 5) + data.substring(0, 2);
		try {
			if (dataCorretta.compareTo(minData) >= 0)
				return true;
			else
				return false;
		} catch (NullPointerException e) {
			return true;
		}
	}

	private boolean maxDataValida(String data, String maxData) {
		String dataCorretta = data.substring(6, 10) + data.substring(3, 5) + data.substring(0, 2);
		try {
			if (dataCorretta.compareTo(maxData) <= 0)
				return true;
			else
				return false;
		} catch (NullPointerException e) {
			return true;
		}
	}

	private boolean minImportoValido(String importo, String minImporto) {
		try {
			if (Float.parseFloat(minImporto) <= Float.parseFloat(importo))
				return true;
			else
				return false;
		} catch (NumberFormatException e) {
			return true;
		}
	}

	private boolean maxImportoValido(String importo, String maxImporto) {
		try {
			if (Float.parseFloat(importo) <= Float.parseFloat(maxImporto))
				return true;
			else
				return false;
		} catch (NumberFormatException e) {
			return true;
		}
	}

	private boolean inNomeValido(String nome, String parolaChiave, boolean inNome) {
		if (inNome) {
			if(nome.indexOf(parolaChiave) != -1)
				return true;
			else 
				return false;
		}
		else
			return true;
	}

	private boolean inDescrizioneValido(String descrizione, String parolaChiave, boolean inDescrizione) {
		if (inDescrizione) {
			if(descrizione.indexOf(parolaChiave) != -1)
				return true;
			else 
				return false;
		}
		else
			return true;
	}
}
