package com.example.easyfamilymanager;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class HomeActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pagina_home);
		
		// bottone pagina indietro
		final AlertDialog.Builder builderconferma = new AlertDialog.Builder(this);
		ImageButton bottoneIndietro = (ImageButton) findViewById(R.id.bottoneIndietro);
		bottoneIndietro.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				builderconferma.setMessage(getResources().getString(R.string.conferma_uscita));
				builderconferma.setCancelable(false);
				builderconferma.setPositiveButton(R.string.si, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						finish();
					}
				});
				builderconferma.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// NON FARE NULLA
					}
				});
				AlertDialog conferma = builderconferma.create();
				conferma.show();
			}
		});
		
		// apro o creo DB per controllare l'utente con cui loggare
		CustomDbHelper databaseHelper = new CustomDbHelper(getApplicationContext());
		SQLiteDatabase db = databaseHelper.getWritableDatabase();
		String sql = "SELECT username FROM utenti WHERE autologin = 1";
		Cursor voce = db.rawQuery(sql, null);

		// vedo se c'� un utente con cui loggare in automatico altrimenti uso quello di default
		VariabiliGlobali globali = (VariabiliGlobali) this.getApplication();
		voce.moveToFirst();
		if (globali.getUtente() == null)
			if (voce.getCount() == 1)
				globali.setUtente(voce.getString(voce.getColumnIndex("username")));
			else
				globali.setUtente(globali.getUtenteDefault());
		TextView campoUtente = (TextView) findViewById(R.id.editUtente);
		campoUtente.setText(globali.getUtente());

		// bottone pagina utenti
		ImageButton bottoneUtenti = (ImageButton) findViewById(R.id.bottoneUtenti);
		bottoneUtenti.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaUtenti = new Intent(HomeActivity.this,UtentiActivity.class);
				startActivity(openPaginaUtenti);
				finish();
			}
		});

		// bottone pagina saldo
		ImageButton bottoneSaldo = (ImageButton) findViewById(R.id.bottoneSaldo);
		bottoneSaldo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaSaldo = new Intent(HomeActivity.this,SaldoActivity.class);
				startActivity(openPaginaSaldo);
				finish();
			}
		});

		// bottone pagina opzioni
		ImageButton bottoneOpzioni = (ImageButton) findViewById(R.id.bottoneOpzioni);
		bottoneOpzioni.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaOpzioni = new Intent(HomeActivity.this,OpzioniActivity.class);
				startActivity(openPaginaOpzioni);
				finish();
			}
		});

		// bottone pagina movimento
		ImageButton bottoneMovimento = (ImageButton) findViewById(R.id.bottoneMovimento);
		bottoneMovimento.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaMovimento = new Intent(HomeActivity.this,MovimentoActivity.class);
				startActivity(openPaginaMovimento);
				finish();
			}
		});

		// bottone pagina nuovo
		ImageButton bottoneNuovo = (ImageButton) findViewById(R.id.bottoneNuovo);
		bottoneNuovo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaNuovo = new Intent(HomeActivity.this,SempliceActivity.class);
				startActivity(openPaginaNuovo);
				finish();
			}
		});

		// bottone pagina rischio
		ImageButton bottoneRischio = (ImageButton) findViewById(R.id.bottoneRischio);
		bottoneRischio.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaRischio = new Intent(HomeActivity.this,RischioActivity.class);
				startActivity(openPaginaRischio);
				finish();
			}
		});
	}
}
