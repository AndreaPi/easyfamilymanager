package com.example.easyfamilymanager;

import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

public class AllarmiActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pagina_allarmi);

		// scrivo il nome utente
		final VariabiliGlobali globali = (VariabiliGlobali) this.getApplication();
		TextView campoUtente = (TextView) findViewById(R.id.editUtente);
		campoUtente.setText(globali.getUtente());

		// bottone pagina home
		ImageButton bottoneHome = (ImageButton) findViewById(R.id.bottoneHome);
		bottoneHome.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaHome = new Intent(AllarmiActivity.this, HomeActivity.class);
				startActivity(openPaginaHome);
				finish();
			}
		});

		// bottone pagina indietro
		ImageButton bottoneIndietro = (ImageButton) findViewById(R.id.bottoneIndietro);
		bottoneIndietro.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaOpzioni = new Intent(AllarmiActivity.this, OpzioniActivity.class);
				startActivity(openPaginaOpzioni);
				finish();
			}
		});

		// setta orario
		CustomDbHelper databaseHelper = new CustomDbHelper(getApplicationContext());
		final SQLiteDatabase db = databaseHelper.getWritableDatabase();
		String sql = "SELECT substr(oraallarme,1,2) ore, substr(oraallarme,4,2) minuti FROM utenti WHERE username = '" + globali.getUtente() + "';";
		Cursor orario = db.rawQuery(sql, null);
		orario.moveToFirst();
		final TimePicker campoOrario = (TimePicker) findViewById(R.id.oraAllarmi);
		campoOrario.setCurrentHour(Integer.parseInt(orario.getString(orario.getColumnIndex("ore"))));
		campoOrario.setCurrentMinute(Integer.parseInt(orario.getString(orario.getColumnIndex("minuti"))));
		
		// bottone orario
		Button bottoneOrario = (Button) findViewById(R.id.bottoneAllarmi);
		bottoneOrario.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				
				// apro il DB e modifico l'orario
				String sql = "UPDATE utenti SET oraallarme = '"
						+ String.format("%02d", campoOrario.getCurrentHour())
						+ ":"
						+ String.format("%02d", campoOrario.getCurrentMinute())
						+ "';";
				db.execSQL(sql);
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.orario_modificato) + 
						String.format("%02d", campoOrario.getCurrentHour()) + ":" + String.format("%02d", campoOrario.getCurrentMinute()), 
						Toast.LENGTH_SHORT).show();
			}
		});
	}
}