package com.example.easyfamilymanager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

public class RischioActivity extends Activity {
	ArrayList<String> idVoci = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pagina_rischio);

		// scrivo il nome utente
		final VariabiliGlobali globali = (VariabiliGlobali) this.getApplication();
		TextView campoUtente = (TextView) findViewById(R.id.editUtente);
		campoUtente.setText(globali.getUtente());

		// bottone pagina home
		ImageButton bottoneHome = (ImageButton) findViewById(R.id.bottoneHome);
		bottoneHome.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaHome = new Intent(RischioActivity.this, HomeActivity.class);
				startActivity(openPaginaHome);
				finish();
			}
		});

		// bottone pagina indietro
		ImageButton bottoneIndietro = (ImageButton) findViewById(R.id.bottoneIndietro);
		bottoneIndietro.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaHome = new Intent(RischioActivity.this, HomeActivity.class);
				startActivity(openPaginaHome);
				finish();
			}
		});

		// prendo le categorie dal DB
		CustomDbHelper databaseHelper = new CustomDbHelper(getApplicationContext());
		final SQLiteDatabase db = databaseHelper.getWritableDatabase();
		String sql = "SELECT idcategoria, nomecategoria FROM categorie WHERE username = '" + globali.getUtente() + "' ORDER BY nomecategoria ASC";
		Cursor voce = db.rawQuery(sql, null);

		// inserisco i dati in un array
		ArrayList<String> voci = new ArrayList<String>();
		voci.add(globali.getCategoriaRoot());
		idVoci.add("null");
		while (voce.moveToNext()) {
			idVoci.add(voce.getString(voce.getColumnIndex("idcategoria")));
			voci.add(voce.getString(voce.getColumnIndex("nomecategoria")));
		}

		// stampo i dati nello spinner tramite adapter
		final Spinner listaCategorie = (Spinner) this.findViewById(R.id.editAggiungi);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, voci);
		listaCategorie.setAdapter(adapter);

		// individuo i campi
		final EditText campoNome = (EditText) this.findViewById(R.id.editNome);
		final DatePicker campoData = (DatePicker) this.findViewById(R.id.editData);
		final EditText campoImporto = (EditText) this.findViewById(R.id.editImporto);
		final EditText campoDescrizione = (EditText) this.findViewById(R.id.editDescrizione);
		final EditText campoProbabilita = (EditText) this.findViewById(R.id.editProbabilita);
		campoProbabilita.setText("10");

		// bottone aggiungi
		final Button bottoneAggiungi = (Button) this.findViewById(R.id.bottoneGestisciCategorie);
		bottoneAggiungi.setEnabled(false);
		bottoneAggiungi.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				// query di inserimento
				String sql = "INSERT INTO movimenti (nomemovimento, datamovimento, descrizione, idcategoria, rischio, username) VALUES ('"
						+ campoNome.getText().toString()
						+ "', '"
						+ String.format("%04d", campoData.getYear())
						+ String.format("%02d", 1 + campoData.getMonth())
						+ String.format("%02d", campoData.getDayOfMonth())
						+ "', '"
						+ campoDescrizione.getText().toString()
						+ "', "
						+ idVoci.get(listaCategorie.getSelectedItemPosition())
						+ ", "
						+ campoProbabilita.getText().toString()
						+ ", '" + globali.getUtente()
						+ "');";
				db.execSQL(sql);

				sql = "INSERT INTO allarmi DEFAULT VALUES;";
				db.execSQL(sql);

				sql = "SELECT MAX(idallarme) FROM allarmi;";
				Cursor idallarme = db.rawQuery(sql, null);
				idallarme.moveToFirst();
				String id = idallarme.getString(0);

				sql = "INSERT INTO rate (nomerata, datarata, importo, idmovimento, idallarme) VALUES ('"
						+ campoNome.getText().toString()
						+ "', '"
						+ String.format("%04d", campoData.getYear())
						+ String.format("%02d", 1 + campoData.getMonth())
						+ String.format("%02d", campoData.getDayOfMonth())
						+ "', "
						+ campoImporto.getText().toString()
						+ ", "
						+ "(SELECT MAX(idmovimento) FROM movimenti), "
						+ "(SELECT MAX(idallarme) FROM allarmi)"
						+ ");";
				db.execSQL(sql);

				// crea allarme
				Calendar data = new GregorianCalendar(campoData.getYear(), campoData.getMonth(), campoData.getDayOfMonth());
				if (globali.getUtente().equals(globali.getUtenteDefault())) {
					data.set(Calendar.HOUR_OF_DAY, Integer.parseInt(VariabiliGlobali.ORARIO_DEFAULT.substring(0, 2)));
					data.set(Calendar.MINUTE, Integer.parseInt(VariabiliGlobali.ORARIO_DEFAULT.substring(3, 5)));
				} else {
					sql = "SELECT substr(oraallarme,1,2) ore, substr(oraallarme,4,2) minuti FROM utenti WHERE username = '" + globali.getUtente() + "';";
					Cursor orario = db.rawQuery(sql, null);
					orario.moveToFirst();
					data.set(Calendar.HOUR_OF_DAY, Integer.parseInt(orario.getString(orario.getColumnIndex("ore"))));
					data.set(Calendar.MINUTE, Integer.parseInt(orario.getString(orario.getColumnIndex("minuti"))));
				}
				if (data.after(Calendar.getInstance())) {
					Intent creaAllarme = new Intent(getApplicationContext(), AlarmReceiverActivity.class);
					creaAllarme.putExtra(getPackageName() + ".parametroId", id);
					PendingIntent allarme = PendingIntent.getActivity(getApplicationContext(), Integer.parseInt(id), creaAllarme, PendingIntent.FLAG_CANCEL_CURRENT);
					AlarmManager managerAllarme = (AlarmManager) getSystemService(Activity.ALARM_SERVICE);
					managerAllarme.set(AlarmManager.RTC_WAKEUP, data.getTimeInMillis(), allarme);
				}

				// pulisco i campi
				campoNome.setText("");
				Calendar dataodierna = Calendar.getInstance();
				campoData.updateDate(dataodierna.get(Calendar.YEAR), dataodierna.get(Calendar.MONTH), dataodierna.get(Calendar.DAY_OF_MONTH));
				campoImporto.setText("");
				campoDescrizione.setText("");
				listaCategorie.setSelection(0);
				campoNome.requestFocus();

				Toast.makeText(getApplicationContext(), R.string.semplice_aggiungi, Toast.LENGTH_SHORT).show();
			}
		});

		// controlli sull'input
		final Button bottoneSegno = (Button) this.findViewById(R.id.bottoneSegno);
		MetodiComuni comuni = new MetodiComuni();
		comuni.controlliComuni(campoNome, campoImporto, bottoneSegno, bottoneAggiungi, getApplicationContext());
		comuni.controlliRischio(campoProbabilita);
	}
}