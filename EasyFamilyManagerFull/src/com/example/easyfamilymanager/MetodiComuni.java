package com.example.easyfamilymanager;

import android.app.Application;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;

public class MetodiComuni extends Application {
	EditText campoNome;
	EditText campoImporto;
	EditText campoRate;

	// controlli sull'input per campi nome ed importo
	public void controlliComuni(EditText cNome, EditText cImporto, Button bottoneSegno, final Button bottone, final Context context) {

		this.campoNome = cNome;
		this.campoImporto = cImporto;

		// bottone segno
		bottoneSegno.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				try {
					float valore = Float.parseFloat(campoImporto.getText().toString());
					valore *= -1;
					campoImporto.setText(Float.toString(valore));
				} catch (NumberFormatException e) {
					campoImporto.setTextColor(context.getResources().getColor(R.color.abc_search_url_text_pressed));
				}
			}
		});

		// controlli sull'input
		campoNome.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (nomeValido() && importoValido())
					bottone.setEnabled(true);
				else
					bottone.setEnabled(false);
			}

			@Override
			public void afterTextChanged(Editable s) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
		});

		campoImporto.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (nomeValido() && importoValido())
					bottone.setEnabled(true);
				else
					bottone.setEnabled(false);

				// coloro l'importo
				try {
					if (Float.parseFloat(campoImporto.getText().toString()) > 0)
						campoImporto.setTextColor(context.getResources().getColor(R.color.positivo));
					if (Float.parseFloat(campoImporto.getText().toString()) < 0)
						campoImporto.setTextColor(context.getResources().getColor(R.color.negativo));
					if (Float.parseFloat(campoImporto.getText().toString()) == 0)
						campoImporto.setTextColor(context.getResources().getColor(R.color.abc_search_url_text_pressed));
				} catch (NumberFormatException e) {
					campoImporto.setTextColor(context.getResources().getColor(R.color.abc_search_url_text_pressed));
				}
			}

			@Override
			public void afterTextChanged(Editable s) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
		});

		// negativo di default al tocco
		campoImporto.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				campoImporto.setText("-");
				return false;
			}
		});
	}

	public void controlliComuni(EditText cNome, EditText cImporto, EditText cRate, Button bottoneSegno, final Button bottone, final Context context) {

		this.campoNome = cNome;
		this.campoImporto = cImporto;
		this.campoRate = cRate;

		// bottone segno
		bottoneSegno.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				try {
					float valore = Float.parseFloat(campoImporto.getText().toString());
					valore *= -1;
					campoImporto.setText(Float.toString(valore));
				} catch (NumberFormatException e) {
					campoImporto.setTextColor(context.getResources().getColor(R.color.abc_search_url_text_pressed));
				}
			}
		});

		// controlli sull'input
		campoNome.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (nomeValido() && importoValido() && rateValido())
					bottone.setEnabled(true);
				else
					bottone.setEnabled(false);
			}

			@Override
			public void afterTextChanged(Editable s) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
		});

		campoImporto.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (nomeValido() && importoValido() && rateValido())
					bottone.setEnabled(true);
				else
					bottone.setEnabled(false);

				// coloro l'importo
				try {
					if (Float.parseFloat(campoImporto.getText().toString()) > 0)
						campoImporto.setTextColor(context.getResources().getColor(R.color.positivo));
					if (Float.parseFloat(campoImporto.getText().toString()) < 0)
						campoImporto.setTextColor(context.getResources().getColor(R.color.negativo));
					if (Float.parseFloat(campoImporto.getText().toString()) == 0)
						campoImporto.setTextColor(context.getResources().getColor(R.color.abc_search_url_text_pressed));
				} catch (NumberFormatException e) {
					campoImporto.setTextColor(context.getResources().getColor(R.color.abc_search_url_text_pressed));
				}
			}

			@Override
			public void afterTextChanged(Editable s) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
		});

		campoRate.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (nomeValido() && importoValido() && rateValido())
					bottone.setEnabled(true);
				else
					bottone.setEnabled(false);
			}

			@Override
			public void afterTextChanged(Editable s) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
		});

		// negativo di default al tocco
		campoImporto.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				campoImporto.setText("-");
				return false;
			}
		});
	}

	// controlli rischio
	public void controlliRischio(final EditText campoProbabilita) {

		campoProbabilita.addTextChangedListener(new TextWatcher() {
			// @overload
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try {
					if (Float.parseFloat(campoProbabilita.getText().toString()) >= 100)
						campoProbabilita.setText("99.99");
				} catch (NumberFormatException e) {
					campoProbabilita.setText("0.");
				}
			}

			@Override
			public void afterTextChanged(Editable s) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
		});
	}

	public boolean nomeValido() {
		if (campoNome.getText().toString().length() == 0)
			return false;
		if (campoNome.getText().toString().contains("["))
			return false;
		return true;
	}

	public boolean importoValido() {
		try {
			if (Float.parseFloat(campoImporto.getText().toString()) != 0)
				;
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	public boolean rateValido() {
		try {
			if (Integer.parseInt(campoRate.getText().toString()) > 1)
				return true;
			else
				return false;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
