package com.example.easyfamilymanager;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CategorieActivity extends Activity {

	private VariabiliGlobali globali;
	private ArrayAdapter<String> adapter;
	private ArrayList<String> voci;
	private ArrayList<String> idVoci;
	private SQLiteDatabase db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pagina_categorie);

		// scrivo il nome utente
		globali = (VariabiliGlobali) this.getApplication();
		TextView campoUtente = (TextView) findViewById(R.id.editUtente);
		campoUtente.setText(globali.getUtente());

		// bottone pagina home
		ImageButton bottoneHome = (ImageButton) findViewById(R.id.bottoneHome);
		bottoneHome.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaHome = new Intent(CategorieActivity.this, HomeActivity.class);
				startActivity(openPaginaHome);
				finish();
			}
		});

		// bottone pagina indietro
		ImageButton bottoneIndietro = (ImageButton) findViewById(R.id.bottoneIndietro);
		bottoneIndietro.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaOpzioni = new Intent(CategorieActivity.this, OpzioniActivity.class);
				startActivity(openPaginaOpzioni);
				finish();
			}
		});

		// prendo le categorie dal DB
		CustomDbHelper databaseHelper = new CustomDbHelper(getApplicationContext());
		db = databaseHelper.getWritableDatabase();
		String sql = "SELECT idcategoria, nomecategoria FROM categorie WHERE username = '" + globali.getUtente() + "' ORDER BY nomecategoria ASC";
		final Cursor voce = db.rawQuery(sql, null);

		// inserisco i dati in un array
		voci = new ArrayList<String>();
		idVoci = new ArrayList<String>();
		while (voce.moveToNext()) {
			idVoci.add(voce.getString(voce.getColumnIndex("idcategoria")));
			voci.add(voce.getString(voce.getColumnIndex("nomecategoria")));
		}

		// stampo i dati nella lista tramite adapter
		final Activity context = this;
		final Spinner listaCategorie = (Spinner) this.findViewById(R.id.editRimuovi);
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, voci);
		listaCategorie.setAdapter(adapter);

		// bottone aggiungi
		final EditText campoNome = (EditText) this.findViewById(R.id.editAggiungi);
		final Button bottoneAggiungi = (Button) findViewById(R.id.bottoneGestisciCategorie);
		bottoneAggiungi.setEnabled(false);
		bottoneAggiungi.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {

				// query di inserimento
				String sql = "INSERT INTO categorie (nomecategoria, username) VALUES "
						+ "('" + campoNome.getText().toString() + "', '" + globali.getUtente() + "');";
				db.execSQL(sql);

				// pulisco il campo e aggiorno lo spinner
				sql = "SELECT MAX(idcategoria) FROM categorie;";
				Cursor nuovo = db.rawQuery(sql, null);
				nuovo.moveToFirst();
				idVoci.add(nuovo.getString(0));
				voci.add(campoNome.getText().toString());
				adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, voci);
				listaCategorie.setAdapter(adapter);
				campoNome.setText("");
				campoNome.requestFocus();
				Toast.makeText(getApplicationContext(), R.string.categoria_aggiunta, Toast.LENGTH_SHORT).show();
			}
		});
		
		// bottone modifica
		final EditText campoNuovo = (EditText) this.findViewById(R.id.editNuovo);
		final Button bottoneModifica = (Button) findViewById(R.id.bottoneModifica);
		bottoneModifica.setEnabled(false);
		bottoneModifica.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {

				// query di modifica
				String sql = "UPDATE categorie SET nomecategoria = '" + campoNuovo.getText().toString() + "' WHERE idcategoria = " + idVoci.get(listaCategorie.getSelectedItemPosition());
				db.execSQL(sql);

				// pulisco il campo e aggiorno lo spinner
				voci.remove(listaCategorie.getSelectedItemPosition());
				voci.add(campoNuovo.getText().toString());
				adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, voci);
				listaCategorie.setAdapter(adapter);
				campoNuovo.setText("");
				campoNome.requestFocus();
				Toast.makeText(getApplicationContext(), R.string.categoria_modificata, Toast.LENGTH_SHORT).show();

			}
		});

		// bottone elimina
		final Button bottoneElimina = (Button) findViewById(R.id.bottoneRimuovi);
		final AlertDialog.Builder builderconferma = new AlertDialog.Builder(this);
		bottoneElimina.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				builderconferma.setMessage(getResources().getString(R.string.conferma_eliminacategoria) + listaCategorie.getItemAtPosition(listaCategorie.getSelectedItemPosition()) + "?");
				builderconferma.setCancelable(false);
				builderconferma.setPositiveButton(R.string.bottone_elimina, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						// query di eliminazione
						String sql = "SELECT idmovimento FROM movimenti WHERE idcategoria = " + idVoci.get(listaCategorie.getSelectedItemPosition());
						Cursor daCancellare = db.rawQuery(sql, null);
						while (daCancellare.moveToNext()) {
							sql = "DELETE FROM rate WHERE idmovimento = '" + daCancellare.getColumnIndex("idmovimento") + "'";
							db.execSQL(sql);
						}
						sql = "DELETE FROM movimenti WHERE idcategoria = " + idVoci.get(listaCategorie.getSelectedItemPosition());
						db.execSQL(sql);
						sql = "DELETE FROM categorie WHERE idcategoria = " + idVoci.get(listaCategorie.getSelectedItemPosition());
						db.execSQL(sql);

						// pulisco il campo e aggiorno lo spinner
						idVoci.remove(listaCategorie.getSelectedItemPosition());
						voci.remove(listaCategorie.getSelectedItemPosition());
						adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, voci);
						listaCategorie.setAdapter(adapter);
						campoNome.setText("");
						campoNuovo.setText("");
						campoNome.requestFocus();
						Toast.makeText(getApplicationContext(), R.string.categoria_eliminata, Toast.LENGTH_SHORT).show();
					}
				});

				builderconferma.setNeutralButton(R.string.bottone_sposta, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						
						//query di spostanmento ed eliminazione
						String sql = "UPDATE movimenti SET idcategoria = null WHERE idcategoria = " + idVoci.get(listaCategorie.getSelectedItemPosition());
						db.execSQL(sql);
						sql = "DELETE FROM categorie WHERE idcategoria = " + idVoci.get(listaCategorie.getSelectedItemPosition());
						db.execSQL(sql);

						// pulisco il campo e aggiorno lo spinner
						idVoci.remove(listaCategorie.getSelectedItemPosition());
						voci.remove(listaCategorie.getSelectedItemPosition());
						adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, voci);
						listaCategorie.setAdapter(adapter);
						campoNome.setText("");
						campoNuovo.setText("");
						campoNome.requestFocus();
						Toast.makeText(getApplicationContext(), R.string.categoria_eliminata, Toast.LENGTH_SHORT).show();
					}
				});

				builderconferma.setNegativeButton(R.string.bottone_annulla, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// NON FARE NULLA
					}
				});
				AlertDialog conferma = builderconferma.create();
				conferma.show();
			}
		});

		// controlli sull'input
		campoNuovo.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (campoNuovo.getText().toString().length() > 0)

					bottoneModifica.setEnabled(true);
				else
					bottoneModifica.setEnabled(false);
			}

			@Override
			public void afterTextChanged(Editable s) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
		});
		
		campoNome.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (campoNome.getText().toString().length() > 0)

					bottoneAggiungi.setEnabled(true);
				else
					bottoneAggiungi.setEnabled(false);
			}

			@Override
			public void afterTextChanged(Editable s) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
		});
	}
}
