package com.example.easyfamilymanager;

import java.io.IOException;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class AlarmReceiverActivity extends Activity {
	private MediaPlayer mMediaPlayer;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		// apro il DB e leggo i dati
		final String id = this.getIntent().getStringExtra(getPackageName() + ".parametroId");
		CustomDbHelper databaseHelper = new CustomDbHelper(getApplicationContext());
		final SQLiteDatabase db = databaseHelper.getWritableDatabase();
		String sql = "SELECT nomerata, idmovimento FROM rate WHERE idallarme = " + id + ";";
		Cursor nome = db.rawQuery(sql, null);
		
		// se non trovo nulla vuol dire che � stato cacellato ed interrompo tutto
		if(nome.moveToFirst()==false){
			finish();
			return;
		}
	
		//scrivo il testo
		setContentView(R.layout.alarm);
		TextView scritta = (TextView) findViewById(R.id.labelTesto);
		sql = "SELECT username FROM movimenti WHERE idmovimento = " + nome.getString(nome.getColumnIndex("idmovimento")) + ";";
		Cursor user = db.rawQuery(sql, null);
		user.moveToFirst();
		scritta.setText(user.getString(user.getColumnIndex("username")) + getResources().getString(R.string.testo_allarme) + nome.getString(nome.getColumnIndex("nomerata")));

		// bottone stop
		Button bottoneStop = (Button) findViewById(R.id.allarmeStop);
		bottoneStop.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				mMediaPlayer.stop();
				finish();
				return false;
			}
		});

		// bottone posponi
		final Spinner tempo = (Spinner) this.findViewById(R.id.editTempo);
		Button Bottoneposponi = (Button) findViewById(R.id.allarmePosponi);
		Bottoneposponi.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				mMediaPlayer.stop();

				// scegli di quanto posporre
				Calendar adesso = Calendar.getInstance();				
				switch (tempo.getSelectedItemPosition()) {
				case 0:
					adesso.add(Calendar.MINUTE, 10);
					break;
				case 1:
					adesso.add(Calendar.HOUR, 1);
					break;
				case 2:
					adesso.add(Calendar.HOUR, 12);
					break;
				case 3:
					adesso.add(Calendar.DAY_OF_MONTH, 1);
					break;
				case 4:
					adesso.add(Calendar.DAY_OF_MONTH, 2);
					break;
				case 5:
					adesso.add(Calendar.DAY_OF_MONTH, 3);
					break;
				case 6:
					adesso.add(Calendar.DAY_OF_MONTH, 7);
					break;
				}

				// ricrea l'alert
				Intent creaAllarme = new Intent(getApplicationContext(), AlarmReceiverActivity.class);
				creaAllarme.putExtra(getPackageName() + ".parametroId", id);
				PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), Integer.parseInt(id), creaAllarme, PendingIntent.FLAG_CANCEL_CURRENT);
				AlarmManager manager = (AlarmManager) getSystemService(Activity.ALARM_SERVICE);
				manager.set(AlarmManager.RTC_WAKEUP, adesso.getTimeInMillis(), pendingIntent);

				// chiudi
				finish();
				return false;
			}
		});
		playSound(this, getAlarmUri());
	}

	// gestione sonoro
	private void playSound(Context context, Uri alert) {
		mMediaPlayer = new MediaPlayer();
		try {
			mMediaPlayer.setDataSource(context, alert);
			final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
				mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
				mMediaPlayer.prepare();
				mMediaPlayer.start();
			}
		} catch (IOException e) {
		}
	}

	// gestione allarme
	private Uri getAlarmUri() {
		Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
		if (alert == null) {
			alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			if (alert == null) {
				alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
			}
		}
		return alert;
	}
}