package com.example.easyfamilymanager;

import android.app.Application;

public class VariabiliGlobali extends Application {
	public static final String UTENTE_DEFAULT = "Guest";
	public static final String CATEGORIA_ROOT = "--Nessuna--";
	public static final String[] precostruite = { "Abbigliamento", "Alimentari", "Animali", "Altro", "Spese per la casa", "Hi-tech", "Istruzione", "Lavoro", "Svaghi", "Trasporti", "Vacanze"};
	public static final String ORARIO_DEFAULT = "09:00";
	private String utente;

	public String getUtenteDefault() {
		return UTENTE_DEFAULT;
	}

	public String getCategoriaRoot() {
		return CATEGORIA_ROOT;
	}

	public String[] getPrecostruite() {
		return precostruite;
	}

	public String getUtente() {
		return utente;
	}

	public void setUtente(String s) {
		utente = s;
	}
}