package com.example.easyfamilymanager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ModificaMovimentoActivity extends Activity {

	Cursor voce;
	ArrayList<String> idVoci;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.modifica_movimento);

		// scrivo il nome utente
		final VariabiliGlobali globali = (VariabiliGlobali) this.getApplication();
		TextView campoUtente = (TextView) findViewById(R.id.editUtente);
		campoUtente.setText(globali.getUtente());

		// bottone pagina home
		ImageButton bottoneHome = (ImageButton) findViewById(R.id.bottoneHome);
		bottoneHome.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaHome = new Intent(ModificaMovimentoActivity.this, HomeActivity.class);
				startActivity(openPaginaHome);
				finish();
			}
		});

		// bottone pagina indietro
		ImageButton bottoneIndietro = (ImageButton) findViewById(R.id.bottoneIndietro);
		bottoneIndietro.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaSaldo = new Intent(ModificaMovimentoActivity.this, SaldoActivity.class);
				startActivity(openPaginaSaldo);
				finish();
			}
		});

		// prendo i parametri
		Intent questaIntent = getIntent();
		final String id = questaIntent.getStringExtra(getPackageName() + ".parametroId");

		// individuo le caselle
		final EditText campoNome = (EditText) findViewById(R.id.editNome);
		final DatePicker campoData = (DatePicker) findViewById(R.id.editData);
		final EditText campoImporto = (EditText) findViewById(R.id.editImporto);
		final EditText campoDescrizione = (EditText) findViewById(R.id.editDescrizione);
		final EditText campoRate = (EditText) this.findViewById(R.id.editRate);
		final Button bottoneSegno = (Button) this.findViewById(R.id.bottoneSegno);

		// leggo DB
		CustomDbHelper databaseHelper = new CustomDbHelper(getApplicationContext());
		final SQLiteDatabase db = databaseHelper.getReadableDatabase();
		final String sql = "SELECT idmovimento, nomemovimento, substr(datamovimento,1,4) anno, substr(datamovimento,5,2) mese, substr(datamovimento,7,2) giorno, descrizione, idcategoria FROM movimenti WHERE idmovimento = " + id;
		voce = db.rawQuery(sql, null);

		// calcolo l'importo totale
		Cursor importi = db.rawQuery("SELECT importo FROM rate WHERE idmovimento = " + id, null);
		float importo = 0;
		while (importi.moveToNext())
			importo += Float.parseFloat(importi.getString(importi.getColumnIndex("importo")));
		if (importo > 0)
			campoImporto.setTextColor(getResources().getColor(R.color.positivo));
		if (importo < 0)
			campoImporto.setTextColor(getResources().getColor(R.color.negativo));

		// riempio le caselle
		voce.moveToFirst();
		campoNome.setText(voce.getString(voce.getColumnIndex("nomemovimento")));
		campoData.updateDate(Integer.parseInt(voce.getString(voce.getColumnIndex("anno"))),
				Integer.parseInt(voce.getString(voce.getColumnIndex("mese"))) - 1,
				Integer.parseInt(voce.getString(voce.getColumnIndex("giorno"))));
		campoImporto.setText(Float.toString(importo));
		campoDescrizione.setText(voce.getString(voce.getColumnIndex("descrizione")));

		// trova e compila rate e ricorrenza
		final Spinner listaRicorrenze = (Spinner) this.findViewById(R.id.editRicorrenza);
		Cursor rate = db.rawQuery("SELECT substr(datarata,1,4) anno, substr(datarata,5,2) mese, substr(datarata,7,2) giorno FROM rate WHERE idmovimento = " + id + " ORDER BY datarata ASC", null);
		campoRate.setText(Integer.toString(rate.getCount()));
		rate.moveToFirst();
		Calendar data1 = new GregorianCalendar(
				Integer.parseInt(rate.getString(rate.getColumnIndex("anno"))),
				Integer.parseInt(rate.getString(rate.getColumnIndex("mese"))),
				Integer.parseInt(rate.getString(rate.getColumnIndex("giorno"))));
		rate.moveToNext();
		Calendar data2 = new GregorianCalendar(
				Integer.parseInt(rate.getString(rate.getColumnIndex("anno"))),
				Integer.parseInt(rate.getString(rate.getColumnIndex("mese"))),
				Integer.parseInt(rate.getString(rate.getColumnIndex("giorno"))));
		if (data1.get(Calendar.YEAR) != data2.get(Calendar.YEAR))
			listaRicorrenze.setSelection(3);
		else if (data1.get(Calendar.MONTH) != data2.get(Calendar.MONTH))
			listaRicorrenze.setSelection(2);
		else if ((data2.get(Calendar.DAY_OF_MONTH) - data1.get(Calendar.DAY_OF_MONTH)) == 1)
			listaRicorrenze.setSelection(0);
		else
			listaRicorrenze.setSelection(1);

		// popolo lo spinner
		Cursor categorie = db.rawQuery("SELECT idcategoria, nomecategoria FROM categorie WHERE username = '" + globali.getUtente() + "' ORDER BY nomecategoria ASC;", null);
		ArrayList<String> voci = new ArrayList<String>();
		voci.add(globali.getCategoriaRoot());
		idVoci = new ArrayList<String>();
		idVoci.add("null");
		String selezionato = null;
		while (categorie.moveToNext()) {
			idVoci.add(categorie.getString(categorie.getColumnIndex("idcategoria")));
			voci.add(categorie.getString(categorie.getColumnIndex("nomecategoria")));
			if (voce.getString(voce.getColumnIndex("idcategoria")) != null) {
				if (voce.getString(voce.getColumnIndex("idcategoria")).equals(categorie.getString(categorie.getColumnIndex("idcategoria"))))
					selezionato = categorie.getString(categorie.getColumnIndex("nomecategoria"));
			}
			else {
				selezionato = globali.getCategoriaRoot();
			}
		}
		final Spinner listaCategorie = (Spinner) this.findViewById(R.id.editAggiungi);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, voci);
		listaCategorie.setAdapter(adapter);

		// setta voce di default
		int posizioneSpinner = adapter.getPosition(selezionato);
		listaCategorie.setSelection(posizioneSpinner);

		// gestisco bottone modifica
		final Button bottoneModifica = (Button) this.findViewById(R.id.bottoneModifica);
		bottoneModifica.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				// query di modifica
				String sql = "UPDATE movimenti SET "
						+ "nomemovimento = '" + campoNome.getText().toString()
						+ "', datamovimento = '" + String.format("%04d", campoData.getYear()) + String.format("%02d", 1 + campoData.getMonth()) + String.format("%02d", campoData.getDayOfMonth())
						+ "', descrizione = '" + campoDescrizione.getText().toString()
						+ "', idcategoria = " + idVoci.get(listaCategorie.getSelectedItemPosition())
						+ " WHERE idmovimento = " + id;
				db.execSQL(sql);
				
				// elimino rate e allarmi
				sql = "SELECT idallarme FROM rate WHERE idmovimento = " + id;
				Cursor idallarme = db.rawQuery(sql, null);
				while (idallarme.moveToNext())
					sql = "DELETE FROM allarmi WHERE idallarme = " + idallarme.getString(idallarme.getColumnIndex("idallarme"));
				sql = "DELETE FROM rate WHERE idmovimento = " + id;
				db.execSQL(sql);
				
				// crea rate
				Calendar data = new GregorianCalendar(campoData.getYear(), 1 + campoData.getMonth(), campoData.getDayOfMonth());
				for (int i = 1; i <= Integer.parseInt(campoRate.getText().toString()); i++) {
					sql = "INSERT INTO allarmi DEFAULT VALUES;";
					db.execSQL(sql);
					sql = "SELECT MAX(idallarme) FROM allarmi;";
					idallarme = db.rawQuery(sql, null);
					idallarme.moveToFirst();
					String id = idallarme.getString(0);
					sql = "INSERT INTO rate (nomerata, datarata, importo, idmovimento, idallarme) VALUES ('"
							+ campoNome.getText().toString() + "[" + i + "]"
							+ "', '"
							+ String.format("%04d", data.get(Calendar.YEAR))
							+ String.format("%02d", data.get(Calendar.MONTH))
							+ String.format("%02d", data.get(Calendar.DAY_OF_MONTH))
							+ "', "
							+ Float.parseFloat(campoImporto.getText().toString()) / Integer.parseInt(campoRate.getText().toString())
							+ ", "
							+ "(SELECT MAX(idmovimento) FROM movimenti), "
							+ "(SELECT MAX(idallarme) FROM allarmi)"
							+ ");";
					db.execSQL(sql);

					// crea allarmi
					if (globali.getUtente().equals(globali.getUtenteDefault())) {
						data.set(Calendar.HOUR_OF_DAY, Integer.parseInt(VariabiliGlobali.ORARIO_DEFAULT.substring(0, 2)));
						data.set(Calendar.MINUTE, Integer.parseInt(VariabiliGlobali.ORARIO_DEFAULT.substring(3, 5)));
					} else {
						sql = "SELECT substr(oraallarme,1,2) ore, substr(oraallarme,4,2) minuti FROM utenti WHERE username = '" + globali.getUtente() + "';";
						Cursor orario = db.rawQuery(sql, null);
						orario.moveToFirst();
						data.set(Calendar.HOUR_OF_DAY, Integer.parseInt(orario.getString(orario.getColumnIndex("ore"))));
						data.set(Calendar.MINUTE, Integer.parseInt(orario.getString(orario.getColumnIndex("minuti"))));
					}
					data.add(Calendar.MONTH, -1);
					if (data.after(Calendar.getInstance())) {
						Intent creaAllarme = new Intent(getApplicationContext(), AlarmReceiverActivity.class);
						creaAllarme.putExtra(getPackageName() + ".parametroId", id);
						PendingIntent allarme = PendingIntent.getActivity(getApplicationContext(), Integer.parseInt(id), creaAllarme, PendingIntent.FLAG_CANCEL_CURRENT);
						AlarmManager managerAllarme = (AlarmManager) getSystemService(Activity.ALARM_SERVICE);
						managerAllarme.set(AlarmManager.RTC_WAKEUP, data.getTimeInMillis(), allarme);
					}
					data.add(Calendar.MONTH, 1);
					
					// rata successiva
					switch (listaRicorrenze.getSelectedItemPosition()) {
					case 0:
						data.add(Calendar.DAY_OF_MONTH, 1);
						break;
					case 1:
						data.add(Calendar.DAY_OF_MONTH, 7);
						break;
					case 2:
						data.add(Calendar.MONTH, 1);
						break;
					case 3:
						data.add(Calendar.YEAR, 1);
						break;
					}
				}

				// messaggio per l'utente
				Toast.makeText(getApplicationContext(), R.string.semplice_modifica, Toast.LENGTH_SHORT).show();
			}
		});

		// bottone elimina
		final Button bottoneElimina = (Button) this.findViewById(R.id.bottoneEliminaDefinitivo);
		final AlertDialog.Builder builderconferma = new AlertDialog.Builder(this);
		bottoneElimina.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				builderconferma.setMessage(getResources().getString(R.string.conferma_eliminarecord));
				builderconferma.setCancelable(false);
				builderconferma.setPositiveButton(R.string.si, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int idlocale) {

						// query di eliminazione
						String sql = "DELETE FROM movimenti WHERE idmovimento = " + id;
						db.execSQL(sql);
						sql = "SELECT idallarme FROM rate WHERE idmovimento = " + id;
						Cursor idallarme = db.rawQuery(sql, null);
						while (idallarme.moveToNext())
							sql = "DELETE FROM allarmi WHERE idallarme = " + idallarme.getString(idallarme.getColumnIndex("idallarme"));
						sql = "DELETE FROM rate WHERE idmovimento = " + id;
						db.execSQL(sql);
						campoNome.setEnabled(false);
						campoData.setEnabled(false);
						campoImporto.setEnabled(false);
						campoDescrizione.setEnabled(false);
						campoRate.setEnabled(false);
						listaCategorie.setEnabled(false);
						listaRicorrenze.setEnabled(false);
						bottoneSegno.setEnabled(false);
						bottoneModifica.setEnabled(false);
						bottoneElimina.setEnabled(false);

						// messaggio per l'utente
						Toast.makeText(getApplicationContext(), R.string.semplice_elimina, Toast.LENGTH_SHORT).show();
					}
				});
				builderconferma.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// NON FARE NULLA
					}
				});
				AlertDialog conferma = builderconferma.create();
				conferma.show();
			}
		});

		// controlli sull'input
		MetodiComuni comuni = new MetodiComuni();
		comuni.controlliComuni(campoNome, campoImporto, campoRate, bottoneSegno, bottoneModifica, getApplicationContext());
	}
}
