package com.example.easyfamilymanager;

import android.text.Editable;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

public class UtentiActivity extends Activity {

	final Context context = this;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pagina_utenti);

		// individuo il DB
		final CustomDbHelper databaseHelper = new CustomDbHelper(getApplicationContext());

		// individuo i campi
		final EditText campoUsername = (EditText) this.findViewById(R.id.editUsername);
		final EditText campoPassword = (EditText) this.findViewById(R.id.editPassword);
		final EditText campoConferma = (EditText) this.findViewById(R.id.editConferma);
		final CheckBox autologin = (CheckBox) findViewById(R.id.autoLogin);
		final Button bottoneElimina = (Button) findViewById(R.id.bottoneEliminaDefinitivo);
		final TextView campoUtente = (TextView) findViewById(R.id.editUtente);

		// scrivo il nome utente
		final VariabiliGlobali globali = (VariabiliGlobali) this.getApplication();
		campoUtente.setText(globali.getUtente());
		if (globali.getUtente().equals(globali.getUtenteDefault())) {
			autologin.setEnabled(false);
			bottoneElimina.setEnabled(false);
		}

		// controllo se devo mettere a true il checkbox
		SQLiteDatabase db = databaseHelper.getReadableDatabase();
		String sql = "SELECT username FROM utenti WHERE autologin = 1";
		Cursor voce = db.rawQuery(sql, null);
		if (voce.moveToFirst())
			autologin.setChecked((voce.getString(voce.getColumnIndex("username"))).equals(globali.getUtente()));

		// bottone pagina home
		ImageButton bottoneHome = (ImageButton) findViewById(R.id.bottoneHome);
		bottoneHome.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaHome = new Intent(UtentiActivity.this, HomeActivity.class);
				startActivity(openPaginaHome);
				finish();
			}
		});

		// bottone pagina indietro
		ImageButton bottoneIndietro = (ImageButton) findViewById(R.id.bottoneIndietro);
		bottoneIndietro.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaHome = new Intent(UtentiActivity.this, HomeActivity.class);
				startActivity(openPaginaHome);
				finish();
			}
		});

		// bottone login
		final Button bottoneLogin = (Button) findViewById(R.id.bottoneLogin);
		bottoneLogin.setEnabled(false);
		bottoneLogin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// controllo se esiste lo username scelto
				if (campoPassword.getText().toString().equals(campoConferma.getText().toString())) {
					SQLiteDatabase db = databaseHelper.getReadableDatabase();
					String sql = "SELECT password FROM utenti WHERE username = '" + campoUsername.getText().toString() + "'";
					Cursor voce = db.rawQuery(sql, null);
					if (voce.moveToFirst()) {

						// controllo username e password ed effettuo il login
						if (voce.getString(voce.getColumnIndex("password")).equals(campoPassword.getText().toString()) || (campoUsername.getText().toString().equals(globali.getUtenteDefault()))) {
							globali.setUtente(campoUsername.getText().toString());
							campoUtente.setText(globali.getUtente());

							// visualizzo l'utente loggato
							if ((globali.getUtente().equals(globali.getUtenteDefault()))) {
								autologin.setEnabled(false);
								bottoneElimina.setEnabled(false);
							} else {
								autologin.setEnabled(true);
								bottoneElimina.setEnabled(true);
							}

							// controllo se devo mettere a true il checkbox
							sql = "SELECT username FROM utenti WHERE autologin = 1";
							voce = db.rawQuery(sql, null);
							if (voce.moveToFirst())
								autologin.setChecked((voce.getString(voce.getColumnIndex("username"))).equals(globali.getUtente()));

							Toast.makeText(getApplicationContext(), R.string.login_successo, Toast.LENGTH_SHORT).show();
							// pulisco i campi
							campoUsername.setText("");
							campoPassword.setText("");
							campoConferma.setText("");
							campoUsername.requestFocus();
						} else {
							Toast.makeText(getApplicationContext(), R.string.errore_password, Toast.LENGTH_SHORT).show();
						}
					} else {
						if (campoUsername.getText().toString().equals(globali.getUtenteDefault())) {
							globali.setUtente(globali.getUtenteDefault());
							campoUtente.setText(globali.getUtenteDefault());
							Toast.makeText(getApplicationContext(), R.string.login_successo, Toast.LENGTH_SHORT).show();
						} else
							Toast.makeText(getApplicationContext(), R.string.errore_username, Toast.LENGTH_SHORT).show();
					}
				}
				else
					Toast.makeText(getApplicationContext(), R.string.conferma_errata, Toast.LENGTH_SHORT).show();
			}
		});

		// bottone crea
		final Button bottoneCrea = (Button) findViewById(R.id.bottoneCrea);
		bottoneCrea.setEnabled(false);
		bottoneCrea.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {

				// controllo se gi� esiste lo username scelto
				if (campoPassword.getText().toString().equals(campoConferma.getText().toString())) {
					SQLiteDatabase db = databaseHelper.getWritableDatabase();
					String sql = "SELECT COUNT (*) FROM utenti WHERE username = '"
							+ campoUsername.getText().toString() + "'";
					Cursor voce = db.rawQuery(sql, null);
					voce.moveToFirst();
					if ((Integer.parseInt(voce.getString(0)) == 0) && !(campoUsername.getText().toString().equals(globali.getUtenteDefault()))) {

						// query di inserimento
						sql = "INSERT INTO utenti (username, password, oraallarme) VALUES ('"
								+ campoUsername.getText().toString() + "', '"
								+ campoPassword.getText().toString() + "', '"
								+ VariabiliGlobali.ORARIO_DEFAULT
								+ "')";
						db.execSQL(sql);

						// pulisco i campi
						campoUsername.setText("");
						campoPassword.setText("");
						campoConferma.setText("");
						campoUsername.requestFocus();
						Toast.makeText(getApplicationContext(), R.string.nuovo_username, Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(getApplicationContext(), R.string.username_esistente, Toast.LENGTH_SHORT).show();
					}
				}
				else
					Toast.makeText(getApplicationContext(), R.string.conferma_errata, Toast.LENGTH_SHORT).show();
			}
		});

		// checkbox autologin
		autologin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SQLiteDatabase db = databaseHelper.getWritableDatabase();
				String sql;
				if (autologin.isChecked()) {

					// se esiste un utente con il flag glielo rimuovo
					sql = "SELECT username FROM utenti WHERE autologin = 1";
					Cursor voce = db.rawQuery(sql, null);
					if (voce.moveToFirst()) {
						sql = "UPDATE utenti SET autologin = 0 WHERE username = '" + voce.getString(voce.getColumnIndex("username")) + "'";
						db.execSQL(sql);
					}

					// aggiungo il flag al DB
					sql = "UPDATE utenti SET autologin = 1 WHERE username = '" + globali.getUtente() + "'";
					db.execSQL(sql);
					Toast.makeText(getApplicationContext(), R.string.autologin_attivato, Toast.LENGTH_SHORT).show();
				} else {

					// rimuovo il flag dal DB
					sql = "UPDATE utenti SET autologin = 0 WHERE username = '" + globali.getUtente() + "'";
					db.execSQL(sql);
					Toast.makeText(getApplicationContext(), R.string.autologin_disattivato, Toast.LENGTH_SHORT).show();
				}
			}
		});

		// bottone elimina con messaggio di conferma
		final AlertDialog.Builder builderconferma = new AlertDialog.Builder(this);
		bottoneElimina.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				builderconferma.setMessage(getResources().getString(R.string.conferma_eliminautente) + globali.getUtente() + "?");
				builderconferma.setCancelable(false);
				builderconferma.setPositiveButton(R.string.si, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						SQLiteDatabase db = databaseHelper.getReadableDatabase();
						String sql = "SELECT idmovimento FROM movimenti WHERE username = '" + globali.getUtente() + "'";
						Cursor daCancellare = db.rawQuery(sql, null);
						while (daCancellare.moveToNext()) {
							sql = "DELETE FROM rate WHERE idmovimento = '" + daCancellare.getColumnIndex("idmovimento") + "'";
							db.execSQL(sql);
						}
						sql = "DELETE FROM movimenti WHERE username = '" + globali.getUtente() + "'";
						db.execSQL(sql);
						sql = "DELETE FROM categorie WHERE username = '" + globali.getUtente() + "'";
						db.execSQL(sql);
						sql = "DELETE FROM utenti WHERE username = '" + globali.getUtente() + "'";
						db.execSQL(sql);
						campoUtente.setText(globali.getUtenteDefault());
						globali.setUtente(globali.getUtenteDefault());
						autologin.setEnabled(false);
						bottoneElimina.setEnabled(false);
					}
				});
				builderconferma.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// NON FARE NULLA
					}
				});
				AlertDialog conferma = builderconferma.create();
				conferma.show();
			}
		});

		// controlli sull'input
		campoUsername.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if ((campoUsername.getText().toString().length() > 0) && (campoUsername.getText().toString() != globali.getUtenteDefault())) {
					bottoneLogin.setEnabled(true);
					bottoneCrea.setEnabled(true);
				}
				else {
					bottoneLogin.setEnabled(false);
					bottoneCrea.setEnabled(false);
				}
			}

			@Override
			public void afterTextChanged(Editable s) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
		});
	}
}