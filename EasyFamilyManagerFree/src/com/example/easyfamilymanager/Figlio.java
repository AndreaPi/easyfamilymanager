package com.example.easyfamilymanager;

public class Figlio {
	private String idmovimento;
	private String nomerata;
	private String datarata;
	private String importorata;
	private String rischio;
	private String descrizione;

	public Figlio(String id, String nome, String data, String importo, String probabilita, String note) {
		this.idmovimento = id;
		this.nomerata = nome;
		this.datarata = data;
		this.importorata = importo;
		this.rischio = probabilita;
		this.descrizione = note;
	}

	public String getId() {
		return this.idmovimento;
	}

	public String getNome() {
		return this.nomerata;
	}

	public String getData() {
		return this.datarata;
	}

	public String getImporto() {
		return this.importorata;
	}

	public String getRischio() {
		return this.rischio;
	}

	public String getDescrizione() {
		return this.descrizione;
	}

	public boolean getFiltrato(Filtro filtro) {
		return filtro.getFiltrato(this);
	}

}
