package com.example.easyfamilymanager;

public class Padre {
	private String nome;
	private String importo;

	public Padre(String nomeCategoria, String importoCategoria) {		
		this.nome = nomeCategoria;
		this.importo = importoCategoria;
	}

	public String getNome() {
		return this.nome;
	}

	public String getImporto() {
		return this.importo;
	}
}