package com.example.easyfamilymanager;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ModificaSempliceActivity extends Activity {

	Cursor voce;
	ArrayList<String> idVoci;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.modifica_semplice);

		// scrivo il nome utente
		final VariabiliGlobali globali = (VariabiliGlobali) this.getApplication();
		TextView campoUtente = (TextView) findViewById(R.id.editUtente);
		campoUtente.setText(globali.getUtente());

		// bottone pagina home
		ImageButton bottoneHome = (ImageButton) findViewById(R.id.bottoneHome);
		bottoneHome.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaHome = new Intent(ModificaSempliceActivity.this, HomeActivity.class);
				startActivity(openPaginaHome);
				finish();
			}
		});

		// bottone pagina indietro
		ImageButton bottoneIndietro = (ImageButton) findViewById(R.id.bottoneIndietro);
		bottoneIndietro.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent openPaginaSaldo = new Intent(ModificaSempliceActivity.this, SaldoActivity.class);
				startActivity(openPaginaSaldo);
				finish();
			}
		});

		// prendo i parametri
		Intent questaIntent = getIntent();
		final String id = questaIntent.getStringExtra(getPackageName() + ".parametroId");

		// individuo le caselle
		final EditText campoNome = (EditText) findViewById(R.id.editNome);
		final DatePicker campoData = (DatePicker) findViewById(R.id.editData);
		final EditText campoImporto = (EditText) findViewById(R.id.editImporto);
		final EditText campoDescrizione = (EditText) findViewById(R.id.editDescrizione);
		final Button bottoneSegno = (Button) this.findViewById(R.id.bottoneSegno);

		// leggo DB
		CustomDbHelper databaseHelper = new CustomDbHelper(getApplicationContext());
		final SQLiteDatabase db = databaseHelper.getReadableDatabase();
		final String sql = "SELECT idmovimento, nomemovimento, substr(datamovimento,1,4) anno, substr(datamovimento,5,2) mese, substr(datamovimento,7,2) giorno, descrizione, idcategoria FROM movimenti WHERE idmovimento = " + id;
		voce = db.rawQuery(sql, null);

		// calcolo l'importo totale
		Cursor importi = db.rawQuery("SELECT importo FROM rate WHERE idmovimento = " + id, null);
		float importo = 0;
		while (importi.moveToNext())
			importo += Float.parseFloat(importi.getString(importi.getColumnIndex("importo")));
		if (importo > 0)
			campoImporto.setTextColor(getResources().getColor(R.color.positivo));
		if (importo < 0)
			campoImporto.setTextColor(getResources().getColor(R.color.negativo));

		// riempio le caselle
		voce.moveToFirst();
		campoNome.setText(voce.getString(voce.getColumnIndex("nomemovimento")));
		campoData.updateDate(Integer.parseInt(voce.getString(voce.getColumnIndex("anno"))),
				Integer.parseInt(voce.getString(voce.getColumnIndex("mese"))) - 1,
				Integer.parseInt(voce.getString(voce.getColumnIndex("giorno"))));
		campoImporto.setText(Float.toString(importo));
		campoDescrizione.setText(voce.getString(voce.getColumnIndex("descrizione")));

		// popolo lo spinner
		Cursor categorie = db.rawQuery("SELECT idcategoria, nomecategoria FROM categorie WHERE username = '" + globali.getUtente() + "' ORDER BY nomecategoria ASC;", null);
		ArrayList<String> voci = new ArrayList<String>();
		voci.add(globali.getCategoriaRoot());
		idVoci = new ArrayList<String>();
		idVoci.add("null");
		String selezionato = null;
		while (categorie.moveToNext()) {
			idVoci.add(categorie.getString(categorie.getColumnIndex("idcategoria")));
			voci.add(categorie.getString(categorie.getColumnIndex("nomecategoria")));
			if (voce.getString(voce.getColumnIndex("idcategoria")) != null) {
				if (voce.getString(voce.getColumnIndex("idcategoria")).equals(categorie.getString(categorie.getColumnIndex("idcategoria"))))
					selezionato = categorie.getString(categorie.getColumnIndex("nomecategoria"));
			}
			else {
				selezionato = globali.getCategoriaRoot();
			}
		}
		final Spinner listaCategorie = (Spinner) this.findViewById(R.id.editAggiungi);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, voci);
		listaCategorie.setAdapter(adapter);

		// setto voce di default
		int posizioneSpinner = adapter.getPosition(selezionato);
		listaCategorie.setSelection(posizioneSpinner);

		// bottone modifica
		final Button bottoneModifica = (Button) this.findViewById(R.id.bottoneModifica);
		bottoneModifica.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				// query di modifica
				String sql = "UPDATE movimenti SET "
						+ "nomemovimento = '" + campoNome.getText().toString()
						+ "', datamovimento = '" + String.format("%04d", campoData.getYear()) + String.format("%02d", 1 + campoData.getMonth()) + String.format("%02d", campoData.getDayOfMonth())
						+ "', descrizione = '" + campoDescrizione.getText().toString()
						+ "', idcategoria = " + idVoci.get(listaCategorie.getSelectedItemPosition())
						+ " WHERE idmovimento = " + id;
				db.execSQL(sql);
				sql = "UPDATE rate SET "
						+ "nomerata = '" + campoNome.getText().toString()
						+ "', datarata = '" + String.format("%04d", campoData.getYear()) + String.format("%02d", 1 + campoData.getMonth()) + String.format("%02d", campoData.getDayOfMonth())
						+ "', importo = " + campoImporto.getText().toString()
						+ " WHERE idmovimento = " + id;
				db.execSQL(sql);

				// messaggio per l'utente
				Toast.makeText(getApplicationContext(), R.string.semplice_modifica, Toast.LENGTH_SHORT).show();
			}
		});

		// bottone elimina
		final Button bottoneElimina = (Button) this.findViewById(R.id.bottoneEliminaDefinitivo);
		final AlertDialog.Builder builderconferma = new AlertDialog.Builder(this);
		bottoneElimina.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				builderconferma.setMessage(getResources().getString(R.string.conferma_eliminarecord));
				builderconferma.setCancelable(false);
				builderconferma.setPositiveButton(R.string.si, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int idlocale) {

						// query di eliminazione
						String sql = "DELETE FROM movimenti WHERE idmovimento = " + id;
						db.execSQL(sql);
						sql = "SELECT idallarme FROM rate WHERE idmovimento = " + id;
						Cursor idallarme = db.rawQuery(sql, null);
						if (idallarme.moveToFirst())
							sql = "DELETE FROM allarmi WHERE idallarme = " + idallarme.getString(idallarme.getColumnIndex("idallarme"));
						sql = "DELETE FROM rate WHERE idmovimento = " + id;
						db.execSQL(sql);
						campoNome.setEnabled(false);
						campoData.setEnabled(false);
						campoImporto.setEnabled(false);
						campoDescrizione.setEnabled(false);
						listaCategorie.setEnabled(false);
						bottoneSegno.setEnabled(false);
						bottoneModifica.setEnabled(false);
						bottoneElimina.setEnabled(false);

						// messaggio per l'utente
						Toast.makeText(getApplicationContext(), R.string.semplice_elimina, Toast.LENGTH_SHORT).show();
					}
				});
				builderconferma.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// NON FARE NULLA
					}
				});
				AlertDialog conferma = builderconferma.create();
				conferma.show();
			}
		});

		// controlli sull'input
		MetodiComuni comuni = new MetodiComuni();
		comuni.controlliComuni(campoNome, campoImporto, bottoneSegno, bottoneModifica, getApplicationContext());
	}
}
