package com.example.easyfamilymanager;

import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
 
public class ExpandableListAdapter extends BaseExpandableListAdapter {
 
    private Activity context;
    private Map<Padre, List<Figlio>> collezione;
    private List<Padre> padri;
 
    public ExpandableListAdapter(Activity context, List<Padre> categorie, Map<Padre, List<Figlio>> collezione) {
        this.context = context;
        this.collezione = collezione;
        this.padri = categorie;
    }
 
    public Object getChild(int groupPosition, int childPosition) {
        return collezione.get(padri.get(groupPosition)).get(childPosition);
    }
 
    public long getChildId(int groupPosition, int childPosition) {
    	final Figlio elemento = (Figlio) getChild(groupPosition, childPosition);
        return Long.parseLong(elemento.getId());
    }    
    
    public String getNome(int groupPosition, int childPosition) {
    	final Figlio elemento = (Figlio) getChild(groupPosition, childPosition);
        return elemento.getNome();
    } 
    
    public float getRischio(int groupPosition, int childPosition) {
    	final Figlio elemento = (Figlio) getChild(groupPosition, childPosition);
        return Float.parseFloat(elemento.getRischio());
    }
          
    public View getChildView(final int groupPosition, final int childPosition,
            boolean isLastChild, View convertView, ViewGroup parent) {
        final Figlio elemento = (Figlio) getChild(groupPosition, childPosition);
        LayoutInflater inflater = context.getLayoutInflater();
         
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.vista_figlio, null);
        }
         
        TextView campoNome = (TextView) convertView.findViewById(R.id.nome);
        TextView campoData = (TextView) convertView.findViewById(R.id.data);
        TextView campoImporto = (TextView) convertView.findViewById(R.id.importo);
         
        campoNome.setText(elemento.getNome());
        campoData.setText(elemento.getData());
        campoImporto.setText(elemento.getImporto());
		if (Float.parseFloat(elemento.getImporto()) > 0)
			campoImporto.setTextColor(context.getResources().getColor(R.color.positivo));
		if (Float.parseFloat(elemento.getImporto()) < 0)
			campoImporto.setTextColor(context.getResources().getColor(R.color.negativo));		
        return convertView;
    }
 
    public int getChildrenCount(int groupPosition) {
        return collezione.get(padri.get(groupPosition)).size();
    }
 
    public Object getGroup(int groupPosition) {
        return padri.get(groupPosition);
    }
 
    public int getGroupCount() {
        return padri.size();
    }
 
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Padre elemento = (Padre) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.vista_padre,null);
        }
        TextView campoNome = (TextView) convertView.findViewById(R.id.nomeCategoria);
        TextView campoParziale = (TextView) convertView.findViewById(R.id.parziale);
        
        campoNome.setTypeface(null, Typeface.BOLD);
        campoNome.setText(elemento.getNome());
        
        campoParziale.setTypeface(null, Typeface.BOLD);
        campoParziale.setText(elemento.getImporto());
		if (Float.parseFloat(elemento.getImporto()) > 0)
			campoParziale.setTextColor(context.getResources().getColor(R.color.positivo));
		if (Float.parseFloat(elemento.getImporto()) < 0)
			campoParziale.setTextColor(context.getResources().getColor(R.color.negativo));
		if (Float.parseFloat(elemento.getImporto()) == 0)
			campoParziale.setTextColor(context.getResources().getColor(R.color.abc_search_url_text_pressed));
        return convertView;
    }
 
    public boolean hasStableIds() {
        return true;
    }
 
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
